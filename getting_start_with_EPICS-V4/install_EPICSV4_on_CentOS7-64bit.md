# 安装 EPICS V4 记录

安装环境：CentOS Linux 7 (64-bit)

参考资料：

- {EPICS V4 主页}(http://epics-pvdata.sourceforge.net/index.html)
- {GETTING STARTED WITH EPICS V4}(http://epics-pvdata.sourceforge.net/gettingStarted.html)
- {EPICS Base github 主页}(https://github.com/epics-base/)

## 1. EPICS V4介绍
EPICS V4 包含四个部分：pvData, pvAccess, pvDatabase and pvaSrv.
由C++和Java两种语言实现，也被Python和Matlab两种语言封装（本文并不涉及）

## 2.Java

### 2.1 配置Java环境
EPICS 需要Java 1.7，CentOS 7 系统自带了openJDK，需要卸载并安装Oracle JDK，具体过程如下：
- 查看openJDK安装信息

```shell
$ rpm -qa | grep java
```

- 卸载系统自带的openJDK

```
(rpm -e --nodeps "上一步骤所列出软件")
$ sudo rpm -e --nodeps java-1.7.0-openjdk-1.7.0.85-2.6.1.2.el7_1.x86_64
$ sudo rpm -e --nodeps java-1.7.0-openjdk-headless-1.7.0.85-2.6.1.2.el7_1.x86_64
$ sudo rpm -e --nodeps javapackages-tools-3.4.1-6.el7_0.noarch
```

- 下载并安装JDK

```
$ sudo rpm -ivh jdk-7u79-linux-x64.rpm 
```

- 检查JDK版本

```
$ java -version
java version "1.7.0_79"
Java(TM) SE Runtime Environment (build 1.7.0_79-b15)
Java HotSpot(TM) 64-Bit Server VM (build 24.79-b02, mixed mode)
```

### 2.2 安装EPICS V4

- 从SourceForge下载软件包，[http://sourceforge.net/projects/epics-pvdata/files/]( http://sourceforge.net/projects/epics-pvdata/files/ )

- 解压

```
$ tar xvfz EPICS-Java-4.4.0.tar.gz
```

可以看到30个左右文件

```
$ ls
caj-1.1.14.jar                      exampleJava-4.0.2-sources.jar
caj-1.1.14-javadoc.jar              jca-2.3.6.jar
caj-1.1.14.pom                      jca-2.3.6-javadoc.jar
caj-1.1.14-sources.jar              jca-2.3.6.pom
directoryService-0.3.3.jar          jca-2.3.6-sources.jar
directoryService-0.3.3-javadoc.jar  pvAccessJava-4.0.3.jar
directoryService-0.3.3.pom          pvAccessJava-4.0.3-javadoc.jar
directoryService-0.3.3-sources.jar  pvAccessJava-4.0.3.pom
easyPVAJava-0.4.3.jar               pvAccessJava-4.0.3-sources.jar
easyPVAJava-0.4.3-javadoc.jar       pvDataJava-4.0.2.jar
easyPVAJava-0.4.3.pom               pvDataJava-4.0.2-javadoc.jar
easyPVAJava-0.4.3-sources.jar       pvDataJava-4.0.2.pom
exampleJava                         pvDataJava-4.0.2-sources.jar
exampleJava-4.0.2.jar               README
exampleJava-4.0.2-javadoc.jar       RELEASE_VERSIONS
exampleJava-4.0.2.pom
```


### 2.3 运行 helloWorld RPC service

- 在当前目录下建立并解压 ```exampleJava文件```

```
$ mkdir exampleJava
$ cd exampleJava
$ jar xf ../exampleJava-2.1.0-sources.jar
```

- 修改启动脚本

```
$ vim /home/yifan/epicsV4/EPICS-Java-4.4.0/exampleJava/services/helloWorld/helloWorld_setup.bash
```

添加

```
WORKSPACE=/home/yifan/epicsV4/EPICS-Java-4.4.0
EXAMPLES=/home/yifan/epicsV4/EPICS-Java-4.4.0/exampleJava
CLASSPATH=${CLASSPATH}:${WORKSPACE}/pvDataJava-4.0.2.jar(一定注意版本号)
CLASSPATH=${CLASSPATH}:${WORKSPACE}/pvAccessJava-4.0.3.jar
```

- 编译

```
$ cd exampleJava/services/helloWorld $ source helloWorld_setup.bash $ javac *.java 
``` 

- 在server端运行

```
$ chmod +x helloServerRunner helloClientRunner 
$ ./helloServerRunner
```

- 在client端运行

```
$ ./helloClientRunner Stefania
Hello Stefania
```

## 3.C++

- 编译EPICS V3，主要过程如下：

```shell
$ mkdir epicsV3
$ cd epicsV3
$ mv ~/Downloads/baseR3.14.12.3.tar .
$ tar xvf baseR3.14.12.3.tar
$ cd base-3.14.12.3/
$ ./startup/EpicsHostArch
linux-x86_64
$ export EPICS_HOST_ARCH=linux-x86_64
$ make
```
- 下载 V4.4.0 C++ 软件包，[http://sourceforge.net/projects/epics-pvdata/files/4.4.0/](http://sourceforge.net/projects/epics-pvdata/files/4.4.0/)

- 编译安装

```
% tar xzf EPICS-CPP-4.4.0.tar.gz
% cd EPICS-CPP-4.4.0
% chmod +x configure.sh
% make configure
% make
```

- 由于pvaPy有单独的配置脚本， 因此需要单独安装. 所依赖的软件可以查看文件```/home/yifan/epicsV4/EPICS-CPP-4.4.0/pvaPy```, 在我编译中，安装了```python-devel,boost,boost-devel```，然后即可编译：

```
% export EPICS4_DIR=`pwd`
% cd pvaPy
% make configure
% make
``` 
