# VASP安装记录

## 关闭 selinux

- 查看SElinux状态
```
[root@flameRD650 InterCompiler]# sestatus 
SELinux status:                 enabled
SELinuxfs mount:                /selinux
Current mode:                   enforcing
Mode from config file:          enforcing
Policy version:                 24
Policy from config file:        targeted
```

- 先临时关闭```selinux```
```
[root@flameRD650 InterCompiler]# setenforce 0
[root@flameRD650 InterCompiler]# sestatus 
SELinux status:                 enabled
SELinuxfs mount:                /selinux
Current mode:                   permissive
Mode from config file:          enforcing
Policy version:                 24
Policy from config file:        targeted
```

- 修改配置文件，下次重启时关闭```SElinux```
```
[root@flameRD650 InterCompiler]# vim /etc/selinux/config
将 SELINUX=enforcing 改为 SELINUX=disabled
```

# 安装l_ccompxe_intel64_2013.5.192
- 解压
- 进入目录
```	
    # cd l_ccompxe_intel64_2013.5.192
```

- 运行
```
	# ./install.sh
```

- 共七个步骤
```
	You will complete the steps below during this installation:
Step 1 : Welcome
Step 2 : License
Step 3 : Activation
Step 4 : Intel(R) Software Improvement Program
Step 5 : Options
Step 6 : Installation
Step 7 : Complete
```
- 激活部分选择 ```2. I want to evaluate my product or activate later```
- 其他均采用默认选项，或选择accept
- 安装好之后的配置，由于需要使得每个用户都能够使用icc，因此，将下面一行内容加入/etc/profile中
```
source /opt/intel/bin/compilervars.sh intel64
```
- 测试是否安装好
```
[zxy@flameRD650 ~]$ icc
icc: command line error: no files specified; for help type "icc -help"
[zxy@flameRD650 ~]$ icpc
icpc: command line error: no files specified; for help type "icpc -help"
```
已经安装成功。

# 安装 l_fcompxe_intel64_2013.5.192
- 与安装l_ccompxe_intel64_2013.5.192的过程完全一致.

- 由于上一步安装中已经修改了```/etc/profile```，因此这一步安装可以省略。

- 完成安装与配置步骤后，测试是否安装成功.
```
[root@flameRD650 l_fcompxe_intel64_2013.5.192]# ifort
ifort: command line error: no files specified; for help type "ifort -help"
```
已经安装成功。

# 安装openmpi

- openmpi为开源软件，可以直接在其官网下载：http://www.open-mpi.org
- 解压 ```tar zxvf openmpi-1.10.1.tar.gz```
- `tar zxvf openmpi-1.10.1.tar.gz```
- 建立文件夹，用于安装openmpi
 ```mkdir ~/openmpi-1.10.1```
 
- ```./configure  --prefix=/home/zxy/software/openmpi-1.10.1 CC=icc CXX=icpc F77=ifort FC=ifort ```
   (必须使用绝对路径)
  （若不加 CC=icc CXX=icpc F77=ifort FC=ifort ，则用gcc编译）

- ```make all install```
(编译时间较长，耐心等待)

- 在/etc/profile内加入如下内容
```
export PATH=/home/zxy/software/openmpi-1.10.1/bin:$PATH 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/zxy/software/openmpi-1.10.1/lib  
export MANPAH=$MANPATH:/home/zxy/software/openmpi-1.10.1/share/man
```    

- 测试是否安装好
```
[zxy@flameRD650 ~]$ which mpirun
~/software/openmpi-1.10.1/bin/mpirun
```

- 运行example
```
[zxy@flameRD650 examples]$ cd /home/zxy/download/openmpi-1.10.1/examples
[zxy@flameRD650 examples]$ make
```
```
[zxy@flameRD650 examples]$ mpirun -np 40 hello_c
Hello, world, I am 0 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 1 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 3 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 4 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 5 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 6 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 9 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 11 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 12 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 13 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 16 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 17 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 19 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 20 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 21 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 25 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 26 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 33 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 34 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 35 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 37 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 38 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 39 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 2 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 8 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 14 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 22 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 23 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 29 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 30 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 31 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 32 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 36 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 15 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 24 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 27 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 28 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 7 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 10 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
Hello, world, I am 18 of 40, (Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015, 125)
```
```
[zxy@flameRD650 examples]$ mpirun -np 40 hello_usempif08
Hello, world, I am 14 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 15 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 17 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 18 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 23 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 28 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  2 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  4 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  5 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  7 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 10 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 12 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  0 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  3 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  6 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 11 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 16 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 21 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 27 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 29 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 32 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 33 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 22 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 25 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 26 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 30 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 31 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 34 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 35 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 36 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 37 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 38 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 39 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  1 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 24 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  8 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am  9 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 13 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 19 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015                                                                                                                                   
Hello, world, I am 20 of 40: Open MPI v1.10.1, package: Open MPI zxy@flameRD650 Distribution, ident: 1.10.1, repo rev: v1.10.0-178-gb80f802, Nov 03, 2015    
```               

# 安装GotoBLAS2
在 VSAP wiki 中，要求安装BLAS，但是网上的多个安装记录安装的是GotoBLAS2，此处存疑。
- 在官网下载，https://www.tacc.utexas.edu/research-development/tacc-software/gotoblas2
- 解压 ```$ tar zxvf GotoBLAS2-1.13.tar.gz```
- ```$ cd GotoBLAS2```
- 执行$ sudo ./quickbuild.64bit时，出现错误
```
../kernel/x86_64/gemm_ncopy_4.S:331: Error: undefined symbol `WPREFETCHSIZE' in operation
```
网上给出的原因是cpu太新，配置文件不识别，需要重新指定一下CPU类型。

- 执行 ```$ make clean```
- 执行 ```$ make BINARY=64 TARGET=NEHALEM```
  这一过程需要联网，可以安装links软件，在命令登录网络通。
  编译成功后，会出现
  ```
  GotoBLAS build complete.
  OS               ... Linux             
  Architecture     ... x86_64               
  BINARY           ... 64bit                 
  C compiler       ... GCC  (command line : gcc)
  Fortran compiler ... GFORTRAN  (command line : gfortran)
  Library Name     ... libgoto2_nehalemp-r1.13.a (Multi threaded; Max num-threads is 40)
  ```
  
